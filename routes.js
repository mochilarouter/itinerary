'use strict'

// const {
//   bodyValidation,
//   headerValidation,
// } = require('./commons/middlewares')
//
// const {
//   createSchema
// } = require('./v1/schemas')

const {
  create
} = require('./v1/factories')

module.exports = (app) => {
  app.post('/v1/itinerary',
    // bodyValidation(createSchema),
    // headerValidation(headerSchema),
    create
  )
}
