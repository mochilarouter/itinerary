'use strict'

const dotenv = require('dotenv')

dotenv.load()

module.exports = {
  app: {
    host: process.env.HOST,
    port: process.env.PORT
  },
  db: {
    url: process.env.DB_URL,
    collection_name: process.env.DB_ITINERARY_COLLECTION
  },
  messaging: {
    url: process.env.RABBIT_URL,
    create_itinerary: {
      queue: {
        name: process.env.CREATE_ITINERARY_QUEUE,
        dead_letter: process.env.CREATE_ITINERARY_QUEUE_DEADLETTER,
        ttl: process.env.CREATE_ITINERARY_QUEUE_TTL
      },
      exchange: {
        name: process.env.CREATE_ITINERARY_EXCHANGE,
        routing_key: process.env.CREATE_ITINERARY_EXCHANGE_KEY,
        dead_letter_name: process.env.CREATE_ITINERARY_EXCHANGE_DEADLETTER,
        dead_letter_routing_key: process.env.CREATE_ITINERARY_EXCHANGE_DEADLETTER_KEY
      }
    }
  }
}
