'use strict'

module.exports = {
  messaging: {
    url: 'localhost',
    create_itinerary: {
      queue: {
        name: process.env.CREATE_ITINERARY_QUEUE,
        dead_letter: process.env.CREATE_ITINERARY_QUEUE_DEADLETTER,
        ttl: process.env.CREATE_ITINERARY_QUEUE_TTL
      },
      exchange: {
        name: process.env.CREATE_ITINERARY_EXCHANGE,
        routing_key: process.env.CREATE_ITINERARY_EXCHANGE_KEY,
        dead_letter_name: process.env.CREATE_ITINERARY_EXCHANGE_DEADLETTER,
        dead_letter_routing_key: process.env.CREATE_ITINERARY_EXCHANGE_DEADLETTER_KEY
      }
    }
  }
}
