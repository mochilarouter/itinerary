'use strict'

const app = require('./app')
const config = require('./config')

const init = async () => {
  const { port, host } = config.app

  // await app.database.connect(config.db.url)

  app.listen(port, () => {
    console.log(`Application is running on ${host}:${port}`)
  })
}

init()
