'use strict'

const register = ({ save, dependencies }) => async ({ itinerary, travelerId }) => {
  const { logger, toText, getUUID } = dependencies
  const data = Object.assign({}, { travelerId }, itinerary, { _id: getUUID() })

  logger.info(`Itinerary to be created ${toText(data)}`)

  try {
    await save(data._id, data)
  } catch (error) {
    logger.fatal(`Error on save Itinerary ${toText(error)}`)
    return new Error({
      name: 'CreateItineraryRedis',
      message: 'Occurred an error on method hmset of database redis',
      details: [error]
    })
  }

  return data
}

module.exports = register
