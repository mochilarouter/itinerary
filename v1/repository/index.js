'use strict'

const register = require('./register')

module.exports = {
  registerItinerary: register
}
