'use strict'

const actions = {
  itinerary: {
    create: 'create_itinerary',
    update: 'update_itinerary'
  }
}

module.exports = api => action => actions[api][action]
