'use strict'

const emitEvent = require('../../commons/services/messaging')
const logger = require('../../commons/lib/logger')
const toText = require('../../commons/lib/to-text')
const getUUID = require('../../commons/lib/get-uuid')

const { promisify } = require('util')

const config = require('../../config')

const { registerItinerary } = require('../repository')

const {
  sendItineraryToQueue,
  onSuccess,
  onError
} = require('../controllers/helpers')

const { create } = require('../controllers')

const { itinerary } = require('../formatters')

module.exports = (request, response) => {
  const { redis } = response.app.database
  const { body: payload, headers } = request
  create({
    services: {
      registerItinerary: registerItinerary({
        save: promisify(redis.hmset).bind(redis),
        dependencies: {
          logger,
          toText,
          getUUID
        }
      }),
      sendItineraryToQueue: 
        sendItineraryToQueue({ emitEvent, action: itinerary('create'), config, logger })
    },
    data: {
      payload,
      headers
    },
    deps: {
      toText,
      logger,
      onSuccess: onSuccess(response),
      onError: onError(response)
    }
  })
}
