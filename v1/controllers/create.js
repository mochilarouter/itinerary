'use strict'

const create = async ({ services, data, deps }) => {
  const {
    sendItineraryToQueue,
    registerItinerary
  } = services
  const { payload, headers } = data
  const { logger, onSuccess, onError, toText } = deps

  logger.info(`Itinerary received ${toText(payload)} for traveler_id ${headers.traveler_id}`)
  let itinerary = {}

  try {
    itinerary = await registerItinerary({ travelerId: headers.traveler_id, itinerary: payload })
  } catch (error) {
    logger.fatal(`Error on registerItinerary ${toText(error)}`)
    const { name, message, details } = error
    return onError({
      status_code: 400,
      name: name,
      message: message,
      details: [details]
    })
  }

  await sendItineraryToQueue(Object.assign(itinerary, { token: headers.token }))

  return onSuccess({
    status_code: 201,
    name: 'Itinerary was sent for creation',
    message: itinerary,
    details: []
  })
}

module.exports = create
