'use strict'

const onError = response => async (data) =>
  response.json(data).status(data.status_code || 400).end()

module.exports = onError
