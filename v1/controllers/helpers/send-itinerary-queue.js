'use strict'

const sendItineraryToQueue = ({ emitEvent, action, config, logger }) => async (message) =>
  emitEvent({ action, message, config, logger })

module.exports = sendItineraryToQueue
