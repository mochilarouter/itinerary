'use strict'

const onSuccess = response => async (data) =>
  response.json(data).status(data.status_code || 200).end()

module.exports = onSuccess
