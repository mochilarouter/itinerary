'use strict'

const sinon = require('sinon')

const create = require('../../v1/controllers/create')

const request = require('./stub/request-stub.json')

const clone = obj => Object.assign({}, obj)

describe('Unit tests', () => {
  let mocks = {
    services: {
      registerItinerary: async ({ travelerId, itinerary }) => {
        if (travelerId) return itinerary

        const error = new Error()
        error.name = 'CreateItineraryRedis'
        error.message = 'Occurred an error on method database redis'
        error.details = []

        throw error
      },
      sendItineraryToQueue: async () => {}
    },
    data: {
      payload: request.payload,
      headers: request.headers
    },
    deps: {
      toText: () => {},
      logger: { info: () => {}, warn: () => {}, fatal: () => {} },
      onSuccess: msg => msg,
      onError: msg => msg
    }
  }

  describe('Create an Itinerary', () => {
    test('Should generate a message to queue', async () => {
      let itineraries = 0

      const mock = clone(mocks)

      mock.services.sendItineraryToQueue = async () => {
        itineraries = itineraries + 1
      }

      mock.deps.logger.info = sinon.spy()
      mock.deps.logger.fatal = sinon.spy()

      const response = await create(mock)

      expect(mock.deps.logger.info.called).toEqual(true)
      expect(mock.deps.logger.info.calledOnce).toEqual(true)
      expect(mock.deps.logger.fatal.notCalled).toEqual(true)

      expect(response.status_code).toEqual(201)
      expect(response.name).toEqual('Itinerary was sent for creation')
      expect(response.message).toEqual(request.payload)
      expect(response.details).toEqual([])

      expect(itineraries).toEqual(1)
    })

    test('Should return an error when the save method not be resolved', async () => {
      let itineraries = 0

      const mock = clone(mocks)

      mock.data.headers = {}
      mock.services.sendItineraryToQueue = async () => {
        itineraries = itineraries + 1
      }

      mock.deps.logger.info = sinon.spy()
      mock.deps.logger.fatal = sinon.spy()

      const response = await create(mock)

      expect(mock.deps.logger.info.called).toEqual(true)
      expect(mock.deps.logger.info.calledOnce).toEqual(true)
      expect(mock.deps.logger.fatal.called).toEqual(true)
      expect(mock.deps.logger.fatal.calledOnce).toEqual(true)

      expect(response.status_code).toEqual(400)
      expect(response.name).toEqual('CreateItineraryRedis')
      expect(response.message).toEqual('Occurred an error on method database redis')
      expect(response.details).toEqual([[]])

      expect(itineraries).toEqual(0)
    })
  })
})
