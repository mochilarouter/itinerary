'use strict'

const moment = require('moment')

const getDate = () => moment().format('MMMM Do YYYY, h:mm:ss a')

const COLORS = {
  RED: '\x1b[31m',
  MAGENTA: '\x1b[35m',
  BLUE: '\x1b[34m',
  WHITE: '\x1b[37m'
}

const logger = {
  info: (obj) => {
    console.log(`${COLORS.WHITE}`, `${getDate()} ${JSON.stringify(obj)}`)
  },
  warn: (obj) => {
    console.log(`${COLORS.RED}`, `${getDate()} ${JSON.stringify(obj)}`)
  },
  fatal: (obj) => {
    console.log(`${COLORS.MAGENTA}`, `${getDate()} ${JSON.stringify(obj)}`)
  }
}

module.exports = logger
