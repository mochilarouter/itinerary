'use strict'

const redis = require('redis').createClient()

redis.on('connect', () => ({}))
redis.on('error', () => ({}))

module.exports = redis
