'use strict'

const uuid = require('uuid')

const getUUID = () => uuid.v4()

module.exports = getUUID
