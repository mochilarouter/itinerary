'use strict'

const toText = obj => JSON.stringify(obj)

module.exports = toText
