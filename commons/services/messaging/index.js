'use strict'

const amqp = require('amqplib')

const DEFAULT_EXCHANGE_TYPE = 'topic'
const DEFAULT_DURABLE = true
const DEFAULT_AUTO_DELETE = false
const DEFAULT_TTL = 10000

const createDeadLetter = ({ queueData, exchangeData, logger }) => async (channel) => {
  try {
    await channel.assertExchange(exchangeData.deadLetter, exchangeData.type, exchangeData.options)
    await channel.assertQueue(queueData.deadLetter, {})
    await channel.bindQueue(queueData.deadLetter, exchangeData.deadLetter, exchangeData.deadLetterKey)
  } catch (error) {
    logger.fatal(`Error on creating dead letter exchange and queue, message: ${error.message}`)
  }
}

const publishMessage = ({ message, queueData, exchangeData, logger }) => async (channel) => {
  try {
    await channel.assertExchange(exchangeData.name, exchangeData.type, exchangeData.options)
    await channel.assertQueue(queueData.name, queueData.options)
    await channel.bindQueue(queueData.name, exchangeData.name, exchangeData.key)

    logger.info(`Publishing message: ${JSON.stringify(message)}`)

    return channel.publish(exchangeData.name, exchangeData.key, Buffer.from(JSON.stringify(message)))
  } catch (error) {
    logger.fatal(`Error on publishing message: ${error.message}`)
  } finally {
    logger.info('Closing channel')
    await channel.close()
  }
}

const getQueueData = messageConfig => ({
  name: messageConfig.queue.name,
  deadLetter: messageConfig.queue.dead_letter,
  options: {
    durable: messageConfig.queue.durable || DEFAULT_DURABLE,
    autoDelete: messageConfig.queue.autoDelete || DEFAULT_AUTO_DELETE,
    arguments: {
      'x-message-ttl': messageConfig.queue.ttl ? parseInt(messageConfig.queue.ttl, 10) : DEFAULT_TTL,
      'x-dead-letter-exchange': messageConfig.exchange.dead_letter_name,
      'x-dead-letter-routing-key': messageConfig.exchange.dead_letter_routing_key
    }
  }
})

const getExchangeData = messageConfig => ({
  name: messageConfig.exchange.name,
  key: messageConfig.exchange.routing_key,
  deadLetter: messageConfig.exchange.dead_letter_name,
  deadLetterKey: messageConfig.exchange.dead_letter_routing_key,
  type: messageConfig.exchange.type || DEFAULT_EXCHANGE_TYPE,
  options: {
    durable: messageConfig.exchange.durable || DEFAULT_DURABLE
  }
})

const emitEvent = async ({ action, message, config, logger }) => {
  const messageConfig = config.messaging[action]
  const queueData = getQueueData(messageConfig)
  const exchangeData = getExchangeData(messageConfig)

  let conn

  try {
    conn = await amqp.connect(config.messaging.url)
    const channel = await conn.createChannel()
    await createDeadLetter({ queueData, exchangeData, logger })(channel)
    await publishMessage({ message, queueData, exchangeData, logger })(channel)
  } catch (error) {
    logger.fatal(`Error on Rabbit connect, message: ${error.message}`)
  } finally {
    await conn.close()
  }
}

module.exports = emitEvent
